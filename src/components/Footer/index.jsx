/* eslint-disable @next/next/no-img-element */
import React from "react";
import Link from "next/link";

const Footer = ({ classText }) => {
  return (
    <footer className={`${classText ? classText : ""}`}>
      <div className="container">
        <div className="row">
          <div className="col-lg-4">
            <div className="item md-mb50">
              <div className="title">
                <h5>Contact Us</h5>
              </div>
              <ul>
                <li>
                  <span className="icon pe-7s-map-marker"></span>
                  <div className="cont">
                    <h6>Official Address</h6>
                    <p>Sakumono (Celebrity Golf Course). Accra, Ghana</p>
                  </div>
                </li>
                <li>
                  <span className="icon pe-7s-mail"></span>
                  <div className="cont">
                    <h6>Email Us</h6>
                    <a href="mailto:sales@cherrytreeghana.com">sales@cherrytreeghana.com</a>
                  </div>
                </li>
                <li>
                  <span className="icon pe-7s-call"></span>
                  <div className="cont">
                    <h6>Call Us</h6>
                    
                    <a href="tel:+233553662366">0553662366</a><br/>
                    <a href="tel:+233544299400">0544299400</a>
                  </div>
                </li>
              </ul>
            </div>
          </div>
          <div className="col-lg-4">
            <div className="item md-mb50">
              <div className="title">
                <h5>Join Our Mailing List</h5>
              </div>
              <ul>
                <li>
                  <div className="subscribe">
                    <input type="text" placeholder="Enter Your Email" />
                    <span className="subs pe-7s-paper-plane"></span>
                  </div>
                </li>
                <li>
                  <div className="social">
                    <Link href="https://facebook.com/cherrytreeghana">
                      <a>
                        <i className="fab fa-facebook-f"></i>
                      </a>
                    </Link>
                    {/* <Link href="#">
                      <a>
                        <i className="fab fa-twitter"></i>
                      </a>
                    </Link> */}
                    <Link href="https://www.instagram.com/cherrytreepropertiesgh">
                      <a>
                        <i className="fab fa-instagram"></i>
                      </a>
                    </Link>
                    {/* <Link href="#">
                      <a>
                        <i className="fab fa-youtube"></i>
                      </a>
                    </Link> */}
                  </div>
                </li>
              </ul>
              {/*<ul>*/}
              {/*  <li>*/}
              {/*    <div className="img">*/}
              {/*      <Link href="/blog-details">*/}
              {/*        <a>*/}
              {/*          <img src="/assets/img/blog/1.jpg" alt="" />*/}
              {/*        </a>*/}
              {/*      </Link>*/}
              {/*    </div>*/}

              {/*    <div className="sm-post">*/}
              {/*      <Link href="/blog-details">*/}
              {/*        <a>*/}
              {/*          <p>*/}
              {/*            The Start-Up Ultimate Guide to Make Your WordPress*/}
              {/*            Journal.*/}
              {/*          </p>*/}
              {/*        </a>*/}
              {/*      </Link>*/}
              {/*      <span className="date">14 Jan 2022</span>*/}
              {/*    </div>*/}
              {/*  </li>*/}
              {/*  <li>*/}
              {/*    <div className="img">*/}
              {/*      <Link href="/blog-details">*/}
              {/*        <a>*/}
              {/*          <img src="/assets/img/blog/2.jpg" alt="" />*/}
              {/*        </a>*/}
              {/*      </Link>*/}
              {/*    </div>*/}
              {/*    <div className="sm-post">*/}
              {/*      <Link href="/blog-details">*/}
              {/*        <a>*/}
              {/*          <p>*/}
              {/*            The Start-Up Ultimate Guide to Make Your WordPress*/}
              {/*            Journal.*/}
              {/*          </p>*/}
              {/*        </a>*/}
              {/*      </Link>*/}
              {/*      <span className="date">14 Jan 2022</span>*/}
              {/*    </div>*/}
              {/*  </li>*/}
              {/*  <li>*/}

              {/*  </li>*/}
              {/*</ul>*/}
            </div>
          </div>
          {/*<div className="col-lg-4">*/}
          {/*  <div className="item">*/}
          {/*    <div className="logo">*/}
          {/*      <img src="/assets/img/logo.jpg" alt="" />*/}
          {/*    </div>*/}

          {/*    <div className="copy-right"></div>*/}
          {/*  </div>*/}
          {/*</div>*/}
        </div>
        <div className="d-flex justify-content-center mt-20">
          <p>
            © 2022 Cherry Tree Properties. All rights reserved.
            {/*Made with passion by <Link href="#">Codehemaa</Link>.*/}
          </p>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
