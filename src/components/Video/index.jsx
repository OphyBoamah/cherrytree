import React from "react";
import ModalVideo from "react-modal-video";
import "react-modal-video/css/modal-video.css";

const Video = () => {
  // React.useEffect(() => {
  //   console.clear();
  // }, []);
  // const [isOpen, setOpen] = React.useState(false);
  return (
    <section className="work-carousel metro section-padding dark ">
      <div className="container-fluid">
        <div className="container">
          <div className="section-head row justify-content-center text-center">
            {/*<div className="col-lg-6 col-md-8 col-sm-10">*/}
            {/*  <h6 className="custom-font wow fadeInDown" data-wow-delay=".3s">*/}
            {/*    Best Works*/}
            {/*  </h6>*/}
            {/*  <h4 className="playfont wow flipInX" data-wow-delay=".5s">*/}
            {/*    Details*/}
            {/*  </h4>*/}
            {/*</div>*/}
          </div>
          <h2 className="text-center mb-62">
            The primary focus of Cherry Tree Properties is to deliver
            well-designed homes that are of good quality, provide the optimum
            investment values and are forever modern.
          </h2>
        </div>
        <div className="row w-100 justify-content-between">
          <div style={{display:"block", paddingBottom:"10%", width: "100%", position: "relative", marginLeft: "4"}}>
            <iframe
              width="300"
              height="430"
              src="https://www.youtube.com/embed/zAFDOlCVg58"
              frameBorder="0"
              allowFullScreen
            ></iframe>
          </div>
          <div style={{display:"block", width: "100%", marginLeft: "4"}}>
            <iframe
            width="300"
              height="430"
              // width="670"
              // height="430"
              src="/assets/img/cherryTree.mp4"
              frameBorder="0"
              allowFullScreen
            ></iframe>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Video;
