/* eslint-disable @next/next/no-img-element */
import React from "react";
import AboutUs1 from "../../components/About-Us1";
import LightLayout from "../../layouts/light";
import Blogs1 from "../../components/Blogs1";
import Contact from "../../components/Contact";
import Intro3 from "../../components/Intro3";
import Testimonials1 from "../../components/Testimonials1";
import Works1 from "../../components/Works1";
import Video from "../../components/Video";

const Home1 = () => {
  React.useEffect(() => {
    document.querySelector("body").classList.add("homepage");
  }, []);
  return (
    <LightLayout footerClass={"mt-30"}>
      <Intro3 />
      <AboutUs1 />
      {/*<VideoWithTestimonials />*/}
      <Works1 />
      <Testimonials1 />
      <Video />
      <Blogs1 />
      <Contact />
    </LightLayout>
  );
};

export default Home1;
